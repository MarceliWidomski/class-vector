/*
 * Vector.cpp
 *
 *  Created on: 26.03.2017
 *      Author: marce
 */

#include <iostream>
#include <cmath>
#include "Vector.h"
using std::cout;
using std::endl;

Vector::Vector() {
	a = 0;
	b = 0;
	c = 0;
}
Vector::Vector(int a, int b, int c) {
	this->a = a;
	this->b = b;
	this->c = c;
}
void Vector::print() const {
	cout << "Vector [" << a << ", " << b << ", " << c << "]" << endl;
}
double Vector::lenght() const {
	return sqrt(pow(this->a, 2) + pow(this->b, 2) + pow(this->c, 2));
}
Vector Vector::operator+(const Vector& other) const {
	Vector resultVector;
	resultVector.setA(this->getA() + other.getA());
	resultVector.setB(this->getB() + other.getB());
	resultVector.setC(this->getC() + other.getC());
	return resultVector;
}
Vector Vector::operator-(const Vector& other) const {
	Vector resultVector;
	resultVector.setA(this->getA() - other.getA());
	resultVector.setB(this->getB() - other.getB());
	resultVector.setC(this->getC() - other.getC());
	return resultVector;
}
Vector Vector::operator*(const int& scalar) const {
	Vector resultVector;
	resultVector.setA(this->getA() * scalar);
	resultVector.setB(this->getB() * scalar);
	resultVector.setC(this->getC() * scalar);
	return resultVector;
}
Vector operator*(const int& scalar, const Vector& factor) {
	Vector resultVector;
	resultVector.setA(factor.getA() * scalar);
	resultVector.setB(factor.getB() * scalar);
	resultVector.setC(factor.getC() * scalar);
	return resultVector;
}
bool Vector::operator==(const Vector& other) const {
	if (this->a == other.a && this->b == other.b && this->c == other.c)
		return 1;
	else
		return 0;
}
bool Vector::operator!=(const Vector& other) const {
	return !(*this == other);
}
bool Vector::operator>(const Vector& other) const {
	if ((this->lenght()) > (other.lenght()))
		return 1;
	else
		return 0;
}
bool Vector::operator>=(const Vector& other) const {
	if ((this->lenght()) >= (other.lenght()))
		return 1;
	else
		return 0;
}
bool Vector::operator<(const Vector& other) const {
	if (*this >= other)
		return 0;
	else
		return 1;
}
bool Vector::operator<=(const Vector& other) const {
	if (*this > other)
		return 0;
	else
		return 1;
}
double Vector::scalarProduct(const Vector& other) const {
	double scalarProduct;
	scalarProduct = this->a * other.getA() + this->b * other.getB() + this->c * other.getC();
	return scalarProduct;
}
Vector Vector::vectorProduct(const Vector& other) const {
	Vector product;
	product.setA(this->b * other.getC() - this->c * other.getB());
	product.setB(this->c * other.getA() - this->a * other.getC());
	product.setC(this->a * other.getB() - this->b * other.getA());
	return product;
}
double Vector::tripleProduct(const Vector& factor1, const Vector& factor2) const {
	double tripleProduct;
	tripleProduct = factor2.getA() * this->b * factor1.getC()
			- factor2.getA() * this->c * factor1.getB()
			- factor2.getB() * this->a * factor1.getC()
			+ factor2.getB() * this->c * factor1.getA()
			+ factor2.getC() * this->a * factor1.getB()
			- factor2.getC() * this->b * factor1.getA();
	return tripleProduct;
}
bool Vector::isOrthogonal (const Vector& other) const{
	if (this->scalarProduct(other))
		return false;
	else
		return true;
}
