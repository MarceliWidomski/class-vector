/*
 * Vector.h
 *
 *  Created on: 26.03.2017
 *      Author: marce
 */

#ifndef VECTOR_H_
#define VECTOR_H_

class Vector {
public:
	Vector();
	Vector(int a, int b = 0, int c = 0);

	void print() const;
	double lenght() const;
	double scalarProduct(const Vector& other) const;
	Vector vectorProduct(const Vector& other) const;
	double tripleProduct(const Vector& factor1, const Vector& factor2) const;
	bool isOrthogonal (const Vector& other) const;
	Vector operator+(const Vector& other) const;
	Vector operator-(const Vector& other) const;
	Vector operator*(const int& scalar) const;
	bool operator==(const Vector& other) const;
	bool operator!=(const Vector& other) const;
	bool operator>(const Vector& other) const;
	bool operator>=(const Vector& other) const;
	bool operator<(const Vector& other) const;
	bool operator<=(const Vector& other) const;

	int getA() const {return a;}
	void setA(int a) {this->a = a;}
	int getB() const {return b;}
	void setB(int b) {this->b = b;}
	int getC() const {return c;}
	void setC(int c) {this->c = c;}

private:
	int a;
	int b;
	int c;
};
Vector operator*(const int& scalar, const Vector& factor);

#endif /* VECTOR_H_ */
