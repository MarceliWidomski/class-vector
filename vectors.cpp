//============================================================================
// Name        : vectors.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Vector.h"
using namespace std;

int main() {

	Vector firstVector(1, 3, -2);
	firstVector.print();
	Vector secondVector(2, 6, -2);
	secondVector.print();
	cout << secondVector.lenght() << endl;
	Vector thirdVector;
	thirdVector = firstVector + secondVector;
	thirdVector.print();
	Vector fourthVector;
	fourthVector = firstVector - secondVector;
	fourthVector.print();
	Vector fifthVector;
	fifthVector = firstVector * 2;
	fifthVector.print();
	Vector sixthVector;
	sixthVector = 2 * firstVector;
	sixthVector.print();
	cout << (fifthVector >= sixthVector) << endl;
	cout << (fifthVector > sixthVector) << endl;
	double scalarProd = firstVector.scalarProduct(secondVector);
	cout << "Scalar product: " << scalarProd << endl;
	Vector vectorProd = firstVector.vectorProduct(secondVector);
	vectorProd.print();
	Vector seventhVector(-1, 1, 1);
	double tripleProd = firstVector.tripleProduct(secondVector, seventhVector);
	cout << "Triple product: " << tripleProd << endl;
	cout << "Is orthogonal: " << firstVector.isOrthogonal(seventhVector) << endl;
	return 0;
}
